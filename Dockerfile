#FROM debian
FROM peeinboerg/saabaseimage
#ENTRYPOINT ["port"]
MAINTAINER Peter Einberger <peter.einberger@ipa.fraunhofer.de>

COPY loader/ /opt/loader/
COPY core/ /opt/core/

#RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list
#RUN echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
#RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
#RUN apt-get update
#RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
#RUN apt-get install -y oracle-java8-installer
#RUN exit
#RUN apt-get install -y git
#RUN java -version
#RUN apt-get install -y maven


WORKDIR /opt/loader/
RUN mvn install
WORKDIR /opt/core/
RUN mvn assembly:assembly

CMD java -jar /opt/core/target/sa_crp-core-0.0.1-SNAPSHOT-jar-with-dependencies.jar debug