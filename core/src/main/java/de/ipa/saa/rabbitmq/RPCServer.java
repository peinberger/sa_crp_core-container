package de.ipa.saa.rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP.BasicProperties;

import de.ipa.saa.crp.Main;
import de.ipa.saa.crp.RuleGenerator;
import de.ipa.saa.crp.Singleton;
import de.ipa.saa.data.Actor;
import de.ipa.saa.data.CommonBaseEvent;
import de.ipa.saa.data.ItemAttribute;
import de.ipa.saa.data.RabbitRequest;
import de.ipa.saa.data.Rule;
import de.ipa.saa.data.RuleRequest;
import de.ipa.saa.data.Sensor;
import de.ipa.saa.data.SensorEvent;
import de.ipa.saa.data.SensorEvent.SensorEventParameter;
import de.ipa.saa.data.SensorParameter;
  
public class RPCServer {
  
  private static final String RPC_QUEUE_NAME = "crp_rpc_queue";
  private Gson gson = new Gson();
  private Main core;
  private Logger logger = LoggerFactory.getLogger(RPCServer.class);
  
  public RPCServer (Main main){
	  this.core = main;
  }
  
  private String rpcServer(String rabbitRequest) {
	RabbitRequest request = gson.fromJson(rabbitRequest, RabbitRequest.class);
	
	switch(request.getAction()){
	case "addCommonBaseEvent":
		logger.info("request: addCommonBaseEvent");
		core.addFact(gson.fromJson(request.getContent(), CommonBaseEvent.class));
		return "OK";
	case "addSensorEvent":
		logger.info("request: addSensorEvent");
		// parse from Sensor Event to CommonBaseEvent
		SensorEvent event = gson.fromJson(request.getContent(), SensorEvent.class);
		core.addFact(new CommonBaseEvent(event));
		return "OK";
	case "addRule":
		logger.info("request: addRule");
		RuleRequest ruleRequest = gson.fromJson(request.getContent(), RuleRequest.class);
		Rule rule = new RuleGenerator(ruleRequest).createRule();
		Singleton.getDataBaseInstance().addRule(rule);
		return "OK";
	case "deactivateRule":
		Singleton.getDataBaseInstance().deactivateRule(request.getContent());
		core.updateRules();
		return "OK";
	case "activateRule":
		Singleton.getDataBaseInstance().activateRule(request.getContent());
		core.updateRules();
		return "OK";
	case "deleteRule":
		Singleton.getDataBaseInstance().deleteRule(request.getContent());
		core.updateRules();
		return "OK";
	default:
		logger.info("RPC Server couldn't identify action "+request.getAction());
	}
	return null;
	  
  }
    
  public void start() {
    Connection connection = null;
    Channel channel = null;
    try {
      ConnectionFactory factory = new ConnectionFactory();
      factory.setHost(Main.getZookeeperPath());
  
      connection = factory.newConnection();
      channel = connection.createChannel();
      
      channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
  
      channel.basicQos(1);
  
      QueueingConsumer consumer = new QueueingConsumer(channel);
      channel.basicConsume(RPC_QUEUE_NAME, false, consumer);
  
      logger.info(" [x] Awaiting RPC requests");
  
      while (true) {
        String response = null;
        
        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
        
        BasicProperties props = delivery.getProperties();
        BasicProperties replyProps = new BasicProperties
                                         .Builder()
                                         .correlationId(props.getCorrelationId())
                                         .build();
        
        try {
          String message = new String(delivery.getBody(),"UTF-8");
  
          logger.info(" [.] received message(" + message + ")");
          response = "" + rpcServer(message);
        }
        catch (Exception e){
          logger.info(" [.] " + e.toString());
          response = "";
        }
        finally {  
          channel.basicPublish( "", props.getReplyTo(), replyProps, response.getBytes("UTF-8"));
  
          channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        }
      }
    }
    catch  (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (connection != null) {
        try {
          connection.close();
        }
        catch (Exception ignore) {}
      }
    }      		      
  }
}

