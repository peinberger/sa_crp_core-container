package de.ipa.saa.rabbitmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import de.ipa.saa.data.EventMessage;

public class EventLogger {
	private String eventStoreAddress, serviceType, serviceName;
	private Boolean DEBUG = false;
	
	private Gson gson = new Gson();
	private String QUEUE_NAME;
	private ConnectionFactory factory = new ConnectionFactory();
	private Connection connection;
	private Channel channel;
	
	public EventLogger(){
		System.out.println("empty Logger created");
	}
	
	public EventLogger(String queue, String address, String serviceType, String serviceName){
		this.QUEUE_NAME = queue;
		this.serviceType = serviceType;
		this.serviceName = serviceName;
		
		if (queue != null && queue.length() > 0) {
			this.factory.setHost(address);
			try {
				this.connection = factory.newConnection();
				this.channel = connection.createChannel();
				this.QUEUE_NAME = queue;
				channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			} catch (IOException | TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			System.out.println("couln't initialize Sender, queue definintion missing");
		}
		
		if(DEBUG)System.out.println("-- Logger: instance created "+eventStoreAddress+" "+serviceType+" "+serviceName);
	}

	public void log(String eventType, String eventContent){
		if(DEBUG)System.out.println("-- Logger: creating log: "+eventType+" "+eventContent);
		EventMessage eventMsg = new EventMessage(serviceName, eventType, eventContent);
		String message = gson.toJson(eventMsg);
		
		if (message != null && message.length() > 0) {
			try {
				channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
				System.out.println(" [x] Log '" + message + "'");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} else {
			System.out.println("Message is not defined");
		}
	}
	
	public void close() throws IOException, TimeoutException{
		channel.close();
		connection.close();
	}
		
	public void setEventStoreAddress(String eventStoreAddress){
		this.eventStoreAddress = eventStoreAddress;
	}
	
	public void setServiceType(String serviceType){
		this.serviceType = serviceType;
	}
	
	public String getServiceType(){
		return serviceType;
	}
	
	public void setServiceName(String serviceName){
		this.serviceName = serviceName;
	}
	
}