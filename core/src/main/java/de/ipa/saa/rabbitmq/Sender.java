package de.ipa.saa.rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;

public class Sender {
	private String QUEUE_NAME;
	private ConnectionFactory factory = new ConnectionFactory();
	private Connection connection;
	private Channel channel;
	private String message;

	// default constructor connects to localhost
	public Sender(String queue) throws IOException, TimeoutException {
		if (queue != null && queue.length() > 0) {
			this.factory.setHost("localhost");
			this.connection = factory.newConnection();
			this.channel = connection.createChannel();
			this.QUEUE_NAME = queue;
		} else {
			System.out.println("couln't initialize Sender, queue definintion missing");
		}
	}

	// use address constructor to specify the rabbitmq server
	public Sender(String address, String queue) throws java.io.IOException, TimeoutException {
		if (queue != null && queue.length() > 0) {
			this.factory.setHost(address);
			this.connection = factory.newConnection();
			this.channel = connection.createChannel();
			this.QUEUE_NAME = queue;
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		} else {
			System.out.println("couln't initialize Sender, queue definintion missing");
		}
	}

	public void setContent(String content) {
		this.message = content;
	}

	public void sendMessage() throws java.io.IOException, TimeoutException {
		if (message != null && message.length() > 0) {
			System.out.println("channel: "+channel);
			System.out.println("message: "+message);
			channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
			System.out.println(" [x] Sent '" + message + "'");

			
		} else {
			System.out.println("Message is not defined");
		}

	}
	
	public void close() throws IOException, TimeoutException{
		channel.close();
		connection.close();
	}
}
