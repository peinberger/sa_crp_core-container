package de.ipa.saa.crp;

import java.util.ArrayList;
import java.util.Arrays;

import de.ipa.saa.data.Rule;
import de.ipa.saa.data.RuleActor;
import de.ipa.saa.data.RuleActorParameter;
import de.ipa.saa.data.RuleRequest;
import de.ipa.saa.data.RuleSensor;
import de.ipa.saa.data.RuleSensorParameter;

/// RuleGenerator constructor needs a CreateRuleRequest object supplied by the gson json parser
public class RuleGenerator {
	private RuleRequest data;
	private String leftHandSide = "", rightHandSide = "";
	private String retract = "";

	/// creates Rules by filling predefined strings with parameters from the
	/// createRule Request
	/// Drools Rule Language Definition can be found at
	/// https://docs.jboss.org/drools/release/6.2.0.Final/drools-docs/html/ch07.html
	public RuleGenerator() {

	}

	public RuleGenerator(RuleRequest data) {
		this.data = data;
	}

	public Rule createRule() {
		if (data != null) {

			// Write Object to Database
			// System.out.println("source: "+data.source);
			// System.out.println("condition: "+data.condition);
			// System.out.println("parameters: "+data.parameters);

			ArrayList<RuleSensor> sensors = new ArrayList<RuleSensor>(Arrays.asList(data.getSensors()));
			ArrayList<RuleActor> actors = new ArrayList<RuleActor>(Arrays.asList(data.getActors()));

			// get source Sensor
			// get SensorIdentifier

			int cbe = 0;
			for (RuleSensor sensor : sensors) {
				//if (sensor.getSingular()) retract+= "retract($cbe"+cbe+")";
				for (RuleSensorParameter param : sensor.getParameters()) {
					String condition = "$cbe" + cbe + " : CommonBaseEvent(Identifier == " + sensor.getId()
							+ " %%condition%%);";
					switch (param.getData_Type()[0].getData_type()) {
					case 1: // boolean
						switch (param.getData_Type()[0].getCondition()) {
						case 0: // false
							condition = condition.replace("%%condition%%", "&& !Boolean.parseBoolean($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue())");
							break;
						case 1: // true
							condition = condition.replace("%%condition%%",
									"&& Boolean.parseBoolean($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue())");
							break;
						}
						break;
					case 2: // int
						switch (param.getData_Type()[0].getCondition()) {
						case 0: // <
							condition = condition.replace("%%condition%%", "&& Integer.parseInt($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) < " + param.getData_Type()[0].getValue());
							break;
						case 1: // =
							condition = condition.replace("%%condition%%", "&& Integer.parseInt($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) == " + param.getData_Type()[0].getValue());
							break;
						case 2: // >
							condition = condition.replace("%%condition%%", "&& Integer.parseInt($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) > " + param.getData_Type()[0].getValue());
							break;
						case 3: // !=
							condition = condition.replace("%%condition%%", "&& Integer.parseInt($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) != " + param.getData_Type()[0].getValue());
							break;
						}
						break;
					case 3: // float
						switch (param.getData_Type()[0].getCondition()) {
						case 0: // <
							condition = condition.replace("%%condition%%", "&& Float.parseFloat($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) < " + param.getData_Type()[0].getValue());
							break;
						case 1: // =
							condition = condition.replace("%%condition%%", "&& Float.parseFloat($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) == " + param.getData_Type()[0].getValue());
							break;
						case 2: // >
							condition = condition.replace("%%condition%%", "&& Float.parseFloat($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) > " + param.getData_Type()[0].getValue());
							break;
						case 3: // !=
							condition = condition.replace("%%condition%%", "&& Float.parseFloat($cbe"+cbe+".getAttribute(\""
									+ param.getName() + "\").getValue()) != " + param.getData_Type()[0].getValue());
							break;
						}
						break;
					case 4: // String
						switch (param.getData_Type()[0].getCondition()) {
						case 0: // equals
							condition = condition.replace("%%condition%%", "&& $cbe"+cbe+".getAttribute(\"" + param.getName()
									+ "\").getValue.equals(\"" + param.getData_Type()[0].getValue() + "\")");
							break;
						case 1: // equals not
							condition = condition.replace("%%condition%%", "&& !$cbe"+cbe+".getAttribute(\"" + param.getName()
									+ "\").getValue.equals(\"" + param.getData_Type()[0].getValue() + "\")");
							break;
						}
						break;
					}
					cbe++;
					// System.out.println("new Condition: ");
					// System.out.println(condition);
					leftHandSide += condition + "\n";
					System.out.println("leftHandSide");
					System.out.println(leftHandSide);
				}
			}
			int ae = 0;
			rightHandSide += "System.out.println(\""+data.getName()+" fired\");\n";
			for (RuleActor actor : actors) {
				String action = "ActionEvent actionEvent" + ae + " = new ActionEvent();\n";
				action += "actionEvent" + ae + ".setActorId(\"" + actor.getId() + "\");\n";
				for (RuleActorParameter param : actor.getParameters()) {
					if(param.getData_type()[0].getData_type() == 1){
						if(param.getData_type()[0].getCondition() == 0){
							action += "actionEvent" + ae + ".addParameter(\"" + param.getName() + "\", \"true\");\n";
						} else {
							action += "actionEvent" + ae + ".addParameter(\"" + param.getName() + "\", \"false\");\n";
						}
					} else {
						action += "actionEvent" + ae + ".addParameter(\"" + param.getName() + "\", \""
								+ param.getData_type()[0].getValue() + "\");\n";
					}
				}
				action += "actionEvent" + ae + ".send();\n";
				rightHandSide += action + "\n"+retract+"\n";
				ae++;
				System.out.println("rightHandSide");
				System.out.println(rightHandSide);
			}
			
			Rule rule = new Rule();
			rule.setActive(false);
			rule.setName(data.getName());
			rule.setErroneous(false);
			rule.setAttributes("");
			rule.setLeftHandSide(leftHandSide);
			rule.setRightHandSide(rightHandSide);
			
			if (rule.getAttributes().isEmpty() || rule.getAttributes() == null || rule.getAttributes() == "NULL"){
				rule.setRuleString(rule.getRuleHeader()+"rule \""+rule.getName()+"\"\n"+rule.getAttributes()+"\n when\n"+rule.getLeftHandSide()+"then \n"+rule.getRightHandSide()+"end \n");
			} else {
				rule.setRuleString(rule.getRuleHeader()+"rule \""+rule.getName()+"\"\n when\n"+rule.getLeftHandSide()+"then \n"+rule.getRightHandSide()+"end \n");
			}
			
			return rule;
			
		} else {
			System.out.println("CreateRuleRequest was empty, rule could not be created");
			return null;
		}
	}
}
