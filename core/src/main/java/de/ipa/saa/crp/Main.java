package de.ipa.saa.crp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.TimerTask;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.time.Calendar;
import org.quartz.impl.calendar.WeeklyCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ipa.saa.data.CommonBaseEvent;
import de.ipa.saa.data.ItemAttribute;
import de.ipa.saa.data.Rule;
import de.ipa.saa.rabbitmq.EventLogger;
import de.ipa.saa.rabbitmq.RPCServer;
import de.ipa.saa.util.CommandLineInterfaceHelper;
import de.ipa.saa.util.DataBase;
import de.ipa.saa.util.QuartzHelper;

public class Main {
	private KieScanner kScanner;
	private KieSession workingSession;
	private KieContainer loaderContainer;
	private KieServices kieServices;
	private HashMap<Integer, FactHandle> sensors = new HashMap<Integer, FactHandle>();
	private static Boolean recoverEvents;
	public static String loaderPath = "";
	private String ruleLoaderPath = loaderPath+"src/main/resources/rules/";
	private String ruleLoaderPomPath = loaderPath+"pom.xml";
	public static String zookeeperPath, mavenPath;
	static Logger logger = LoggerFactory.getLogger(Main.class);
    	
	public static final void main(String[] args) {
		
        try {
            CommandLine cli = new CommandLineInterfaceHelper().create(args);
            
            if( cli.hasOption("debug")){
            } else {
            }
            
            // specify path of the loader component or if not: set it to src/main/resources/rules/
            if( cli.hasOption( "loaderpath" ) ) {
                String loaderPathParam = cli.getOptionValue( "loaderpath" );
                if (!loaderPathParam.substring(loaderPathParam.length()-1).equals("/")){
                	loaderPath = loaderPathParam+"/";
                }
                else {
                	loaderPath = loaderPathParam;
                }
                logger.info("loaderpath set to: "+loaderPath);
            } else {
                logger.info("loaderpath not set, exiting");
                System.exit(0);
            }
            if( cli.hasOption( "address" ) ) {
                zookeeperPath = cli.getOptionValue( "address" );
                logger.info("zookeeperPath set to: "+zookeeperPath);
            } else {
                zookeeperPath =  "localhost";
                logger.info("zookeeperPath set to: "+zookeeperPath);
            }
            if( cli.hasOption( "mvnpath" ) ) {
                mavenPath = cli.getOptionValue( "mvnpath" );
                logger.info("mavenPath set to: "+mavenPath);
            } else {
            	if (System.getProperty("os.name").contains("Windows")){
        			mavenPath = "/Program Files/Apache/maven";	
        		} else {
        			mavenPath = "/usr/share/maven";	
        		}
                logger.info("mavenPath set to: "+mavenPath);
            }
            if(cli.hasOption("recoverevents")){
            	recoverEvents = true;
            	logger.info("Event backup is activated");
            } else {
            	recoverEvents = false;
            }
            if(cli.hasOption("clear")){
            	logger.info("EventStore will be cleared");
            	try {
					Singleton.getDataBaseInstance().clearEventStore();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
            }
        
        } catch (ParseException e) {
            System.err.println( "Parsing failed.  Reason: " + e.getMessage() );
        }
	    
	    try {
			Main core = Singleton.getCoreInstance();
			Singleton.getDataBaseInstance(zookeeperPath);
			core.init();
			new RPCServer(core).start();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	    
	}
	
	// initializes all necessary components for Drools
	// create kieContainer and kieSession for the Core project and the RulesLoader project
	// the internal session contains an empty rule that validates that everything loads correctly
	// add the KieScanner for the RulesLoader version 0.0.1-Snapshot. This needs to be in the local git repository
	// initialize the Quartz Calendar for Weekdays to the RulesLoader session
	// restore working memory from event database
	// fire all rules for both Containers
	// add a app-shutdown-hook, so that the working memory Backup starts before closing the application
	// add a timer to regularly create a working memory backup (currently every 5min)
	private void init() {
		//EventLogger logger = Singleton.createLoggerInstance("eventStore", zookeeperPath, "crp-core", "mastermind");
		kieServices = KieServices.Factory.get();
		
		loaderContainer = kieServices.newKieContainer(kieServices.newReleaseId("de.ipa.saa", "sa_crp-rules-loader", "0.0.1-SNAPSHOT"));
		kScanner = kieServices.newKieScanner(loaderContainer);
		workingSession = loaderContainer.newKieSession("loader");
		
		org.quartz.Calendar quartzWeeklyCal = new WeeklyCalendar();
		Calendar weekDayCal = QuartzHelper.quartzCalendarAdapter(quartzWeeklyCal);
		workingSession.getCalendars().set("weekday", weekDayCal);
		
		if (recoverEvents){
			try {
			    restoreEvents();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } catch (ClassNotFoundException e){
	            e.printStackTrace();
	        }
		}
		
		updateRules();
		workingSession.fireAllRules();
		
		
		Runtime.getRuntime().addShutdownHook(new Thread("app-shutdown-hook") {
			@Override
			public void run() {
				logger.info("sa_crp-core shutting down");
				if (recoverEvents) storeEvents();
			}
		});
		
		if (recoverEvents) new java.util.Timer().scheduleAtFixedRate(new backupTask(), 300000, 300000);

	}
	
	class backupTask extends TimerTask{
	    public void run(){
	    	logger.info(LocalDateTime.now()+" Event backup started");
	        storeEvents();
	    }
	}

	
	// creates a new DRL-Ressource with createRulesLoaderDrl() and after completion creates the project with invokeMaven()
	// scans for a new version of the RulesLoader project and adds the rules to the working memory
	// after the new DRL-Ressource is added fireAllRules to make sure new rules that are true, are fired
	public void updateRules() {
		logger.info("updateRules started");
		createRulesLoaderDrl();
		
		// invoke Maven clean install process of the RulesLoader Project with the ruleLoaderPomPath
		MavenInvoker invoker = new MavenInvoker();
		invoker.start();
	}
	
	public void scanAndFire(){
		kScanner.scanNow();
		workingSession.fireAllRules();
	}
	
	// Creates a drl Ressource string from the rules stored in the Rule Database while skipping the rules that are deativated or errneous
	// Deletes the drl Ressource file stored in ruleLoaderPath
	// Creates new drl Ressource file in ruleLoaderPath
	private void createRulesLoaderDrl() {
		// alle Rules als ArrayList holen
		// fuer jedes Element eine drl mit Regelnamen als drl-Namen erstellen
		logger.info("createRulesLoaderDrl started");
		ArrayList<Rule> rules = null;
		DataBase db = Singleton.getDataBaseInstance();
		
		try {
			rules = db.getRules();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		logger.debug("about to delete rules in "+ruleLoaderPath);
		//delete all Rule-files
		File file = new File(ruleLoaderPath);
		logger.debug("canWrite: "+file.canWrite());
		logger.debug("canRead: "+file.canRead());
		try {
			FileUtils.cleanDirectory(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.debug("deleted rules in "+ruleLoaderPath);
		
		if (!rules.isEmpty()){
			for (Rule rule : rules){
				if(rule.isActive()){
					logger.info("adding rule: "+rule.getName());
					try {
						PrintWriter writer = new PrintWriter(ruleLoaderPath+rule.getName()+".drl", "UTF-8");
						writer.print(rule.getRule());
						writer.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else if (rule.isErroneous()) {
					logger.info("rule "+rule.getName()+" is errneous and will be skipped");
				} else {
					logger.debug("rule "+rule.getName()+" is deactivated and will be skipped");
				}
			}
		} else {
			logger.info("no Rules in the DataBase");
		}
		
	}
	

	/// adds a Common Base Event to the Knowledge Base
    /// If the Identifier of the Common Base Event is not in the Knowledge Base, the CBE is added as new fact
	/// If the Identifier of the Common Base Event is already in the Knowledge Base, the corresponding fact gets updated
	public void addFact(CommonBaseEvent event) {
		for (ItemAttribute item : event.getAttributes()){
			logger.debug("Fact Attribute: "+item.getName()+" - "+item.getType()+" - "+item.getValue());
		}
		if (sensors.containsKey(event.getIdentifier())){
			//fact is already in knowledgebase, updating fact
			logger.info("found fact "+event.getIdentifier()+" in knowledgebase, updating it");
			FactHandle fact = sensors.get(event.getIdentifier());
			workingSession.update(fact, event);
			sensors.put(event.getIdentifier(), fact);
			logger.debug("fact updated");
		} else {
			//fact is not in knowledgebase, adding fact
			logger.info("couldnt find fact "+event.getIdentifier()+" in knowledgebase, adding it");
			FactHandle fact = workingSession.insert(event);
			sensors.put(event.getIdentifier(), fact);
			logger.debug("fact added");
		}
		logger.info("knowledgebase contains "+(workingSession.getObjects().size())+" facts");
		workingSession.fireAllRules();
	}

	/// fetches facts from current working memory and saves them in the event database
	public void storeEvents(){
	    ArrayList<CommonBaseEvent> facts = new ArrayList<CommonBaseEvent>();
	    DataBase db = Singleton.getDataBaseInstance();
	    Collection<? extends Object> workingMemory =  workingSession.getObjects();
	    
	    if (workingMemory.size() > 0){
	        for (Object obj : workingMemory){
	            CommonBaseEvent fact = (CommonBaseEvent) obj;
	            facts.add(fact);
	        }
	        
	        try {
	            db.createEventStore(facts);
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        
	    } else {
	    	logger.info("storeEvents failed: workingMemory is empty");
	    }
	}
	
	/// fetches facts from event database and adds them to the working memory
	public void restoreEvents() throws SQLException, ClassNotFoundException{
        ArrayList<CommonBaseEvent> facts = new ArrayList<CommonBaseEvent>();
        DataBase db = Singleton.getDataBaseInstance();
        
        facts = db.getEvents();
        for (CommonBaseEvent fact : facts){
            addFact(fact);
        }
        
	}

	public static String getZookeeperPath() {
		return zookeeperPath;
	}

	public static String getLoaderPath() {
		return loaderPath;
	}

	public String getRuleLoaderPath() {
		return ruleLoaderPath;
	}

	public String getRuleLoaderPomPath() {
		return ruleLoaderPomPath;
	}

	public String getMavenPath() {
		return mavenPath;
	}

	public KieScanner getkScanner() {
		return kScanner;
	}
}
