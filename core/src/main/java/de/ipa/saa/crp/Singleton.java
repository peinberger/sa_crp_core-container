package de.ipa.saa.crp;

import de.ipa.saa.crp.Singleton;
import de.ipa.saa.rabbitmq.DataBaseClient;
import de.ipa.saa.rabbitmq.EventLogger;
import de.ipa.saa.util.DataBase;

/// Singleton Pattern implementation

public class Singleton {
	private static Main coreInstance;
	private static DataBase dbInstance;
	private static DataBaseClient dbClientInstance;
	private static EventLogger loggerInstance;

	private Singleton() {
	}

	/// Core Singleton Instance
	public static Main getCoreInstance() {
		if (Singleton.coreInstance == null) {
			Singleton.coreInstance = new Main();
		}
		return Singleton.coreInstance;
	}

	/// DataBaseClient Singleton Instance
		public static DataBaseClient getDataBaseClientInstance() {
			if (Singleton.dbClientInstance == null) {
				Singleton.dbClientInstance = new DataBaseClient();
			}
			return Singleton.dbClientInstance;
		}
		
		public static DataBaseClient getDataBaseInstance(String address) {
			if (Singleton.dbClientInstance == null) {
				try {
					Singleton.dbClientInstance = new DataBaseClient(address);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return Singleton.dbClientInstance;
		}
	
	/// DataBase Singleton Instance
	public static DataBase getDataBaseInstance() {
		if (Singleton.dbInstance == null) {
			Singleton.dbInstance = new DataBase();
		}
		return Singleton.dbInstance;
	}

	public static EventLogger createLoggerInstance(String queue, String address,String type, String name) {
		if (Singleton.loggerInstance == null) {
			Singleton.loggerInstance = new EventLogger(queue, address, type, name);
		}
		return Singleton.loggerInstance;
	}

	public static EventLogger getLoggerInstance() {
		if (Singleton.loggerInstance == null) {
			Singleton.loggerInstance = new EventLogger();
		}
		return Singleton.loggerInstance;
	}
}
