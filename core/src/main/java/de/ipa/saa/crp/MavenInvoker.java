package de.ipa.saa.crp;

import java.io.File;
import java.util.Collections;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MavenInvoker implements Runnable{
	private Main core = Singleton.getCoreInstance();
	private Thread invoker;
	
	public MavenInvoker(){
		
	}
	
	@Override
	public void run() {
		Logger logger = LoggerFactory.getLogger(MavenInvoker.class);
	    logger.info("invokeMaven started");
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile( new File(core.getRuleLoaderPomPath()) );
		request.setGoals( Collections.singletonList( "clean install -Dskips --quiet" ) );
		Invoker invoker = new DefaultInvoker();
		invoker.setMavenHome(new File(core.getMavenPath()));
		
		try {
			InvocationResult result = invoker.execute(request);
			if ( result.getExitCode() != 0 )
			{
			    throw new IllegalStateException( "Build failed." );
			} else {
				logger.info(result.toString());
			}
			invoker.execute( request );
			core.scanAndFire();
		} catch (MavenInvocationException e) {
			logger.info("invoke maven  hat ein Problem");
			e.printStackTrace();
		}
		
	}
	
	public void start(){
		invoker = new Thread (this);
		invoker.start();
	}

}
