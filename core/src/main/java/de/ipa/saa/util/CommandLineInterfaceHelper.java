package de.ipa.saa.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;



public class CommandLineInterfaceHelper {
    private CommandLineParser parser;
    private Options options;
    
    public CommandLineInterfaceHelper(){
        
    }
    
    public CommandLine create(String[] args) throws ParseException{
        this.parser = new DefaultParser();
        this.options = new Options();
        
        options.addOption(new Option("debug", "show debug information"));
        options.addOption(new Option("address", true, "define the zookeeper addrees"));
        options.addOption(new Option("loaderpath", true, "define the loader path"));
        options.addOption(new Option("mvnpath",true, "define the path to maven"));
        options.addOption(new Option("recoverevents","backup events regularly and recover them on startup"));
        options.addOption(new Option("clear","clear all backed up events and start clean"));
        options.addOption(new Option("help", "show this help"));
        
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "SenseAndAct-ComplexRuleEngine", options );
        
        CommandLine cmd = parser.parse( options, args);
        return cmd;
    }
    
    
}
