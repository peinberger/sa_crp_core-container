package de.ipa.saa.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.HttpAsyncClient;
import org.apache.http.util.EntityUtils;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;

import com.google.gson.Gson;

import de.ipa.saa.crp.Singleton;
import de.ipa.saa.data.Actor;
import de.ipa.saa.data.RabbitRequest;
import de.ipa.saa.rabbitmq.EventLogger;

/// ActionEvent is created to invoke actor actions
/// Actor behavior is defined in the rule
public class ActionEvent {
	private String actorId;
	private String action;
	private String address;
	private EventLogger logger = Singleton.getLoggerInstance();
	private Gson gson = new Gson();
	private Actor actor;
	private ArrayList<ActionParameter> parameters = new ArrayList<ActionParameter>();
	
	
	public ActionEvent() {

	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public void send() {

		this.generate();
		System.out.println("trying to send "+action+" to "+address);
		Client client = ClientBuilder.newClient();
		WebTarget sender = client.target(address);
		sender.request(MediaType.APPLICATION_JSON).post(Entity.json(action));
		client.close();

		
		
		
		/**
		AsyncHttpClient asyncHttpClient = new DefaultAsyncHttpClient();
		asyncHttpClient.preparePost(address).setBody(gson.toJson(action)).execute();
		try {
			asyncHttpClient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}**/
	}
	
	public void sendPlain(){
		System.out.println("trying to plane send "+action+" to "+address);
		Client client = ClientBuilder.newClient();
		WebTarget sender = client.target(address);
		sender.request(MediaType.APPLICATION_JSON).post(Entity.json(action));
		client.close();
	}

	public void generate() {
		RabbitRequest request = new RabbitRequest();
		request.setAction("getActor");
		request.setContent(actorId);
		try {
			String response = Singleton.getDataBaseClientInstance().call(gson.toJson(request));
			System.out.println("database response: " + response);
			Actor requestedActor = gson.fromJson(response, Actor.class);
			System.out.println();
			if (requestedActor != null) {
				this.actor = requestedActor;
				this.action = actor.getFunctionCall();
				this.address = actor.getAddress();

				for (ActionParameter param : parameters) {
					System.out.println("trying to replace " + param.getName() + " with " + param.getValue());
					this.action = action.replace("\\%" + param.getName() + "\\%", param.getValue());
				}
			}

		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}

	public void addParameter(String name, String value) {
		this.parameters.add(new ActionParameter(name, value));
	}

	public class ActionParameter {
		String name, value;

		public ActionParameter(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
