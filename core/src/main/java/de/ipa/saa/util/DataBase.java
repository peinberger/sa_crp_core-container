package de.ipa.saa.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import de.ipa.saa.data.CommonBaseEvent;
import de.ipa.saa.data.ItemAttribute;
import de.ipa.saa.data.Rule;

public class DataBase {
	
	public DataBase() {
		
	}
	
	/// creates a ArrayList composed of Rule-Objects from the Rule-database
	/// table columns are active, erroneous, name, attributes, lefthand, righthand
	/// creates all Rules with a human readable ruleString with all necessary linebreaks ready for drl-file creation
	public ArrayList<Rule> getRules() throws ClassNotFoundException, SQLException {
		
		ArrayList<Rule> rules = new ArrayList<Rule>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		Statement statement = null;
		connection = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
		connection.setAutoCommit(false);
		try {
			
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM RULES;");

			while (resultSet.next()) {
				Rule rule = new Rule();
				rule.setActive(resultSet.getBoolean("active"));
				rule.setErroneous(resultSet.getBoolean("erroneous"));
				rule.setName(resultSet.getString("name"));
				rule.setAttributes(resultSet.getString("attributes"));
				rule.setLeftHandSide(resultSet.getString("lefthand"));
				rule.setRightHandSide(resultSet.getString("righthand"));
				
				String attributes = resultSet.getString("attributes");
				
				if (attributes.isEmpty() || attributes == null || attributes == "NULL"){
					rule.setRuleString(rule.getRuleHeader() 
					        + "rule \"" + rule.getName() 
					        + "\"\nwhen\n" 
					        + rule.getLeftHandSide() + "\n" 
					        + "then\n" 
					        + rule.getRightHandSide() + "\n" 
					        + "end\n");
				} else {
					rule.setRuleString(rule.getRuleHeader() 
					        + "rule \"" + rule.getName() + "\"\n" 
					        + attributes 
					        + "\nwhen\n" 
					        + rule.getLeftHandSide() + "\n" 
					        + "then\n" 
					        + rule.getRightHandSide() + "\n" 
					        + "end\n");
				}
				rules.add(rule);
			}
			
			resultSet.close();
			statement.close();
			connection.close();
			return rules;
			
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("no Rules returned");
		return rules;
	}
	
	/// activates a Rule by Name
	public void activateRule(String rule){
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "UPDATE RULES set active = 1 where name='"+rule+"';";
	      stmt.executeUpdate(sql);
	      c.commit();

	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("rule "+rule+" activated");
	}
	
	/// deactivates a Rule by Name
	public void deactivateRule(String rule){
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "UPDATE RULES set active = 0 where name='"+rule+"';";
	      stmt.executeUpdate(sql);
	      c.commit();

	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("rule "+rule+" deactivated");
	}
	
	/// marks a Rule as erroneous by Name
	public void markRuleAsErroneous(String badRule) {
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "UPDATE RULES set erroneous = 1 where id='"+badRule+"';";
	      stmt.executeUpdate(sql);
	      c.commit();

	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("rule "+badRule+" deactivated");
	}
	
	/// creates a ArrayList composed of CommonBaseEvents from the Event-database 
	public ArrayList<CommonBaseEvent> getEvents() throws ClassNotFoundException, SQLException {
	    ArrayList<CommonBaseEvent> events = new ArrayList<CommonBaseEvent>();
        Class.forName("org.sqlite.JDBC");
        Connection connection = null;
        Statement eventStatement = null;
        Statement attributeStatement = null;
        connection = DriverManager.getConnection("jdbc:sqlite:eventDB.db");
        connection.setAutoCommit(true);
        try {
            
            eventStatement = connection.createStatement();
            ResultSet eventSet = eventStatement.executeQuery("SELECT * FROM events;");

            while (eventSet.next()) {
                CommonBaseEvent event = new CommonBaseEvent();
                ArrayList<ItemAttribute> attributes = new ArrayList<ItemAttribute>();
                event.setIdentifier(eventSet.getInt("identifier"));
                event.setSender(eventSet.getString("sensor"));
                event.setOccured(eventSet.getString("occured"));
                attributeStatement = connection.createStatement();
                ResultSet attributeSet = attributeStatement.executeQuery("SELECT * FROM attributes WHERE \"parentEvent\"==\""+event.getIdentifier()+"\"");
                while (attributeSet.next()){
                    ItemAttribute attribute = new ItemAttribute();
                    attribute.setName(attributeSet.getString("name"));
                    attribute.setValue(attributeSet.getString("value"));
                    attribute.setType(attributeSet.getString("type"));
                    attributes.add(attribute);
                }
                ItemAttribute attr[] = new ItemAttribute[attributes.size()];
                event.setAttributes(attributes.toArray(attr));
                events.add(event);
            } 
            eventSet.close();
            eventStatement.close();
            connection.close();
            return events;
            
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("no Events returned");
        return events;
	}
	
	public void addRule(Rule rule){
		Connection c = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      PreparedStatement pstmt = c.prepareStatement("INSERT INTO RULES VALUES (NULL,?,?,?,?,?,?);");
	      pstmt.setString(1, rule.getName());
	      pstmt.setBoolean(2, rule.getActive());
	      pstmt.setBoolean(3, rule.getErroneous());
	      pstmt.setString(4, rule.getAttributes());
	      pstmt.setString(5, rule.getLeftHandSide());
	      pstmt.setString(6, rule.getRightHandSide());
	      pstmt.executeUpdate();
	      c.commit();
	      
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	}
	
	/// creates CommonBaseEvent entries in the Event-database
	/// deletes table entries before creating new entries
    public void createEventStore(ArrayList<CommonBaseEvent> facts) throws ClassNotFoundException{
	    Class.forName("org.sqlite.JDBC");
	    Connection connection = null;
        
        try{
            connection = DriverManager.getConnection("jdbc:sqlite:eventDB.db");
            connection.setAutoCommit(true);
            Statement eventStatement = connection.createStatement();
            Statement attributeStatement = connection.createStatement();
            //delete table content
            eventStatement.executeUpdate("DELETE FROM events;");
            attributeStatement.executeUpdate("DELETE FROM attributes;");
            System.out.println("facts size: "+facts.size());
    	    for (CommonBaseEvent fact : facts){
    	        System.out.println("creating fact");
    	        String eventSql = "INSERT INTO events (identifier, sensor, occured) "
                        + "VALUES (\""+fact.getIdentifier()+"\", \""+fact.getSender()+"\", \""+fact.getOccured()+"\")";
    	        eventStatement.executeUpdate(eventSql);
    	        
    	        for (ItemAttribute attribute : fact.getAttributes()){
    	            String attrSql = "INSERT INTO attributes (parentEvent, name, value, type) "
                            + "VALUES (\""+fact.getIdentifier()+"\", \""+attribute.getName()+"\", \""+attribute.getValue()+"\", \""+attribute.getType()+"\")";
    	            attributeStatement.executeUpdate(attrSql);
    	        }
    	    }    	    
    
            attributeStatement.close();
            eventStatement.close();
            connection.close();
        
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
	}
    
    public void clearEventStore() throws ClassNotFoundException{
	    Class.forName("org.sqlite.JDBC");
	    Connection connection = null;
        
        try{
            connection = DriverManager.getConnection("jdbc:sqlite:eventDB.db");
            connection.setAutoCommit(true);
            Statement eventStatement = connection.createStatement();
            Statement attributeStatement = connection.createStatement();
            //delete table content
            eventStatement.executeUpdate("DELETE FROM events;");
            attributeStatement.executeUpdate("DELETE FROM attributes;");
            attributeStatement.close();
            eventStatement.close();
            connection.close();
        
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

	public void deleteRule(String content) {
		Connection c = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:ruleDB.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      PreparedStatement pstmt = c.prepareStatement("DELETE FROM rules WHERE name=\""+content+"\";");
	      pstmt.executeUpdate();
	      c.commit();
	      
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
		
	}
}
